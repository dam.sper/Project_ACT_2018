#!/usr/bin/env python3

import decision_tree as dctree

# from sklearn.ensemble import RandomForestRegressor
import numpy as np
import matplotlib.pyplot as plt


def main(n_estimator=5):
    parameters = {
        "min_samples_split": 3,
        "presort": True,
    }

    trained_forest, train, test = randomForestregressor(
        n_estimator, parameters)

    z_photo_predicted, scores = predictForest(trained_forest, test)

    z_photo_final = forestScore(z_photo_predicted, test["out"], 'median')

    plotResults(z_photo_final, test)


def randomForestregressor(n_estimator, parameters):
    trees = []
    n_estimator = np.int(n_estimator)

    all_train, all_test, unknown = dctree.generateData(
        noise=False, colors=False)
    all_train["in"] = dctree.cleanupData(all_train["in"])

    for i in range(n_estimator):
        train, test, tmp = dctree.generateData(data=all_train, noise=True,
                                               colors=True, rand_seed=i)
        tree_fit = dctree.trainTree(train, parameters)
        trees.append(tree_fit)

    tmp, all_test, unknown = dctree.generateData(noise=True, colors=True)
    all_test["in"] = dctree.cleanupData(all_test["in"])

    return trees, all_train, all_test


def predictForest(trees, test):
    scores = []
    z_photo = []
    for i in range(len(trees)):
        tree = trees[i]

        z_tmp = tree.predict(test['in'])
        z_photo.append(z_tmp)

    return np.array(z_photo), np.array(scores)


def forestScore(z_photo, z_spec, operator):
    '''
    Operator shoud be median or mean as a string and not anything else
    '''
    if operator == 'median':
        z_photo = np.median(z_photo, axis=0)
    elif operator == 'mean':
        z_photo = np.mean(z_photo, axis=0)
    else:
        raise NameError(
            "Wrong string entered -- check wether it is mean or median")

    u = np.sum((z_spec - z_photo)**2)
    v = np.sum((z_spec - z_spec.mean())**2)
    score = 1. - u/v
    print('The score of the forest is %f' % score)
    return z_photo


def plotResults(z, test):
    z_spec = test["out"]
    dz = z - z_spec
    # check distribution dz
    plt.figure()
    plt.hist(dz, 50, range=(-0.5, 0.5))
    plt.xlabel(r"$\Delta z$")
    plt.ylabel("Number of Cases")

    # check dz as function of z
    plt.figure()
    plt.plot(z_spec, z-z_spec, "o", ms=1)
    plt.ylim([-0.05, 0.05])
    plt.xlabel("Redshift Spectro")
    plt.ylabel(r"$\Delta z$")

    # check z_ml as function of z_spectro
    plt.figure()
    plt.plot(z_spec, z, "o", ms=1)
    m = min(z_spec.min(), z.min())
    M = max(z_spec.max(), z.max())
    plt.plot([m, M], [m, M], '--k', lw=1, alpha=0.8)
    plt.xlabel("Redshift Spectroscopic")
    plt.ylabel("Redshift Machine Learning")

    plt.show()


if __name__ == '__main__':
    # for i in range(10):
    #    main(n_estimator=i+1)

    main()
