#!/usr/bin/env python3

import make_learning_set as mls

# standard libraries
import numpy as np
import matplotlib.pyplot as plt


def main():
    filenames = ["aegis-n2.deblend.v5.1.cat", "cosmos-1.deblend.v5.1.cat"]
    ratio_nan = .5
    skip = ["id"]
    colors = [("U", "G", "F"), ("G", "I", "F"), ("U", "Ks", "F"), ("G", "R", "F"), 
              ("U", "R", "F"),("z", "Ks", "F"), ("U", "I", "F")]

    data = mls.readData(filenames)

    # cleanup data
    data = mls.removeNaN(data, ratio_nan)

    data = mls.removeColumns(data, skip)

    z_spec = data['z_spec']

    c = (z_spec>0.)#&(z_spec<1.5)

    # add colors
    data = mls.addColor(data, colors)

    plotColor(data, colors, c)
    
def plotColor(data, colors, c):
    for i in range(-len(colors),0):
        plt.plot(data['z_spec'][c], data[data.keys()[i]][c], '+k')
        plt.xlabel('Spectroscopic redshift')
        plt.ylabel(data.keys()[i])
        plt.show()
    
if __name__ == "__main__":
    main()
