# Project_ACT_2018
Project for the Advanced Computing Techniques course of the spring semester of 2018 at EPFL.

PREACHER stands for Photometric REdshifts and mACHine lEarning: Random Forest Regressor.


You will find the report as report.pdf (along with its tex file report.tex). All the images are in the directory imgs and can be generated with the python scripts (which are mentioned in the report).