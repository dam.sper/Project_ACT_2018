%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Short Sectioned Assignment
% LaTeX Template
% Version 1.0 (5/5/12)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% Original author:
% Frits Wenneker (http://www.howtotex.com)
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[paper=a4, fontsize=11pt]{scrartcl} % A4 paper and 11pt font size

\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs
\usepackage[english]{babel} % English language/hyphenation
\usepackage{amsmath,amsfonts,amsthm} % Math packages
\usepackage{listings}
\usepackage{graphicx}
\usepackage{natbib}
\usepackage{hyperref}
\usepackage{float}


\lstset{language=Python}
\usepackage{fancyhdr} % Custom headers and footers
\pagestyle{fancyplain} % Makes all pages in the document conform to the custom headers and footers
\fancyhead{} % No page header - if you want one, create it in the same way as the footers below
\fancyfoot[L]{} % Empty left footer
\fancyfoot[C]{} % Empty center footer
\fancyfoot[R]{\thepage} % Page numbering for right footer
\renewcommand{\headrulewidth}{0pt} % Remove header underlines
\renewcommand{\footrulewidth}{0pt} % Remove footer underlines
\setlength{\headheight}{13.6pt} % Customize the height of the header

\numberwithin{equation}{section} % Number equations within sections (i.e. 1.1, 1.2, 2.1, 2.2 instead of 1, 2, 3, 4)
\numberwithin{figure}{section} % Number figures within sections (i.e. 1.1, 1.2, 2.1, 2.2 instead of 1, 2, 3, 4)
\numberwithin{table}{section} % Number tables within sections (i.e. 1.1, 1.2, 2.1, 2.2 instead of 1, 2, 3, 4)

\setlength\parindent{0pt} % Removes all indentation from paragraphs - comment this line for an assignment with lots of text

%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------

\newcommand{\horrule}[1]{\rule{\linewidth}{#1}} % Create horizontal rule command with 1 argument of height

\title{	
\normalfont \normalsize 
\textsc{Ecole Polytechnique F\'ed\'erale de Lausanne} \\ [25pt] % Your university, school and/or department name(s)
\horrule{0.5pt} \\[0.4cm] % Thin top horizontal rule
\huge Photometric Redshift through Random Forest \\ % The assignment title
\horrule{2pt} \\[0.5cm] % Thick bottom horizontal rule
}

\author{Loic Hausammann \& Damien Sp\'erone} % Your name

\date{\normalsize\today} % Today's date or a custom date

\begin{document}

\maketitle % Print the title

%----------------------------------------------------------------------------------------
%	Intro
%----------------------------------------------------------------------------------------

\section{Introduction}
Current cosmological observations requires accurate measurement of the redshift as it describes at the same time, the distance from us and the light emission time.
Unfortunately, the measurement requires precise spectroscopy which is often not done and replaced by photometry\footnote{Photometry consists in the observation in a few different filters}.
Due to the recent interest in machine learning, people started to look at its efficiency to predict a redshift based only on a few bands \citep{2015MNRAS.449.1275H,2017arXiv170904205B}.
Here we are presenting our work on predicting an accurate photometric redshift based on a random forest.

%----------------------------------------------------------------------------------------
%	Method
%----------------------------------------------------------------------------------------

\section{Method and Analysis}

This section will be split in three parts.
In the first, we will study the inputs, the second one a single tree and the last one the full random forest.

\subsection{Inputs}
We are using one of the best datasets available today in astrophysics : the NEWFIRM Medium-Band Survey (NMBS; \citealt{2011ApJ...735...86W}). This dataset covers 2 of the most studied parts of the sky : the AEGIS\footnote{\url{http://aegis.ucolick.org/}} and COSMOS\footnote{\url{http://cosmos.astro.caltech.edu/}} fields. Hence, we have at our disposal the fluxes of 58.880 galaxies in very different bands (or filters) between Far-UV (150 $nm$) and Far-Infrared (24 $\mu m$). As there are 20 bands in common between those 2 fields, we focus on those ones and ditch the rest. In the end, we have fluxes for 2 bands in UV, 5 bands in the visible, 8 in the near-IR and 5 in the IR and far-IR.


Included in these more than 50.000 galaxies are 5.000 galaxies with a confirmed spectroscopic redshift. We are using those as training and test sets for our regressor. In Figure \ref{fig:z_spec}, we have the distribution of galaxies according to their spectroscopic redshift. We can see that the distribution is quite balanced in the range of redshifts of the study: $]0.0,1.5[$. Even though there seems to be an excess of galaxies  between 0.2 and 0.4 and 0.6 and 0.8, it will turn to be not have a significant influence. We have a lack of galaxies at high redshifts, this might limit the performance of our regressor, but it is needed to go as far in redshift as possible in order to be able to obtain the redshift of as most galaxies as possible.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=12cm]{imgs/z_spec_full_range}
    \caption{Repartition of galaxies in function of their spectroscopic redshift. The lack of galaxies above $z<1.5$ is clearly visible.
    \label{fig:z_spec}}
  \end{center}
\end{figure} 

The data is read and processed in {\tt make\_learning\_set.py}. In this routine, the 2 different catalogues are appended. Then the columns with a numbers to NaNs ratio below 0.5 are dropped and some useless columns are deleted as well, such as the id or the position of the galaxies on the images. The ratio of fluxes (colors) are added. Random gaussian noise is added based on the errors on the measure of fluxes for all our bands. Finally, the spectroscopic set is divided into a training and a testing set.

\subsection{Single Tree}
We decided to use \lstinline{sklearn.tree.DecisionTreeRegressor} for computing the photometric redshift and implemented all the functions in {\tt decision\_tree.py}.

The first analysis was on the tree parameters and their influence has been found to be negligible (at reasonable value), therefore the default parameters were chosen for our trees.

As the redshift will translate the spectrum of an object, looking only at the intensity of the object is not enough, the tree needs to compare bands together.
Usually, people use the difference between two magnitudes.
Unfortunately, this approach has the large disadvantages of using logarithmic scale which tends to compress the data together and being ill defined at 0 which reduce the amount of data usable.
Therefore we performed a series of test with the difference of magnitudes (average score of 0.69) and with a series with the ratio of fluxes got a small improvement (average score of 0.72).

In Figure \ref{fig:features}, we looked at the influence of each features are only showing the most important ones.
In the three most important features, two of them are fluxes and one is a ratio of fluxes.
The R (for red) and U (for UV) are filters used to analyse the stars where the red one is for old and light stars and the U for young and massive stars.
Due to the evolution of star formation in the universe, theses filters tend to differentiate high redshift (large fraction of blue galaxies) and low redshift (large fraction of red galaxies)
The last one is the ratio between CH1 and CH2 that operates in the infra red and measure the quantity of dust that can attenuate the intensity of optical light.
The abundance of dust is known to be larger at high redshift and is a logical choice to design a photometric redshift measurement.
Therefore our decision tree seems to be mainly focused on the intensity of the fluxes and the attenuation from the dust.

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=12cm]{imgs/features}
    \caption{Importance of each features for the single decision tree.
      The label star\_flag indicates if the object is a star or not, a label with '/' indicates a ratio of fluxes and without it a flux.
      \label{fig:features}}
  \end{center}
\end{figure}

In Figure \ref{fig:single_dist} and \ref{fig:single_comp} we show the results obtained with our single decision tree.
We are able to find the redshift without systematic bias and with a reasonable absolute error (about 0.1), but this is still far away from the science requirement.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=12cm]{imgs/single_dist}
    \caption{Distribution of the difference between the redshift computed with machine learning and the spectroscopic redshift.
      The distribution is symmetric around 0 and thus we do not have a systematic bias.\label{fig:single_dist}}
  \end{center}
\end{figure}


\begin{figure}[H]
  \begin{center}
    \includegraphics[width=12cm]{imgs/single_comp_redshift}
    \caption{The dashed line represent the perfect case.
      The low redshift part ($z<1.5$) is well computed while the high redshift part has some trouble due to the low number of data in this area.
    \label{fig:single_comp}}
  \end{center}
\end{figure}


\subsection{Random Forest}
Since our single tree is working and giving us some nice results, we decided to use it for the random forest. The difference between trees is done on the galaxy selection and the gaussian noise of each training sample. We then take the median of the predicted redshifts and of the different scores to get our final values. The random forest is implemented in {\tt random\_forest.py}.

Our results with the random forest are visible in Figures \ref{fig:rf_dist} and \ref{fig:rf_comp}. It is worth noting that this is only for our spectroscopic sample, which means that this sample is very well reproduced with a score o 0.93. Here again no specific bias arises and the mean absolute error has dropped significantly with just 5 iterations. We manage to recover the initial spectroscopic redshifts with a mean absolute error below 0.03 and even below 0.015 for a majority of our sample.

The next step is now to apply this random forest regressor to the rest of the sample.

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=12cm]{imgs/rf_dist}
    \caption{Distribution of the difference between the redshift computed with machine learning and the spectroscopic redshift.\label{fig:rf_dist}}
  \end{center}
\end{figure}

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=12cm]{imgs/rf_comp_redshift}
    \caption{The dashed line represent the perfect case. The low redshift part ($z<1.5$) is still well computed. The high redshift one is better constrained this time.\label{fig:rf_comp}}
  \end{center}
\end{figure}


%----------------------------------------------------------------------------------------
%	Results
%----------------------------------------------------------------------------------------

\section{Results}
The NMBS dataset also contains the photometric redshifts derived using a method called EAZY \citep{Brammer2008}. This method is based on spectral energy distribution fitting, it uses spectra derived from stellar evolution models and adapted for galaxies. It computes a grid of values for all the templates and all the filters used corresponding to different sets of parameters such as metallicity, redshift or dust attenuation. Then the parameters for each galaxy are derived from a chi-squared fitting of the input fluxes with the grid parameters and in the end it gives the photometric redshift.

Our goal is to compare the redshifts obtained with our random forest regressor to the ones obtained with the previous method. In order to do that, we first consider the photometric redshifts of the catalogue as the "good" redshifts, which we are basing our analysis on.

To get a full set of galaxies with photometric redshifts, we first remove the ones with a spectroscopic redshift, we then remove sources with a {\it star\_flag} = 1 parameter, which indicates potential stars that would pollute our sample. We apply the same treatment as our spectroscopic sample: removal of useless columns and of those with a numbers to NaNs ration below 0.5, etc. Everything is implemented in {\tt final\_results.py}.

Since we saw previously that 5 iterations are enough to reach a good precision, we use the same parameters for this final sample. The results we obtain are really promising since we recover with a very good precision the vast majority of the redshifts derived with EAZY. The mean absolute error is of the order of 0.3, which is a good precision for a photometric redshift. 

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=12cm]{imgs/rf_dist_final_results1}
    \caption{Distribution of the difference between the redshift computed with our method and the photometric redshifts.\label{fig:rf_dist_final}}
  \end{center}
\end{figure}

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=12cm]{imgs/rf_comp_redshift_final_results1}
    \caption{Comparison of the 2 photometric-based methods. The dashed line represent the perfect case.
      The higher redhsift ($z<1.5$) part is less constrained than the low redshift part, due to the lack of spectroscopic data to constrain our trees.\label{fig:rf_comp_final}}
  \end{center}
\end{figure}

%----------------------------------------------------------------------------------------
%	Conclusion
%----------------------------------------------------------------------------------------

\section{Conclusion}
Machine learning is an excellent tool to estimate very quickly the redshift of close galaxies.
With the high quality dataset used, we were able to reach an accuracy of the order of 0.01 with a small random forest and could be improved with a larger dataset (in number of galaxies and/or maybe bands) or other machine learning techniques (such as neural network).
Due to the lack of high redshift galaxies, this approach is not yet available for them, but with the upcoming of the new generation of telescopes, it is expected to arrive soon.

\bibliographystyle{apalike}
\bibliography{report}

\end{document}
