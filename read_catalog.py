#!/usr/bin/env python3

import numpy as np
import pandas as pd
from astropy.io import ascii


def read_catalogs(filepath):
    '''
    Function that reads an ASCII file and convert it into a pandas dataframe
    Replaces all -99. with NaN
    '''
    data = ascii.read(filepath)
    # print(data.keys())
    data_array = data.as_array()
    pdata = pd.DataFrame(data_array, index=range(len(data_array)),
                         columns=data.keys())

    pdata = pdata.replace(-99., np.nan)

    return pdata


def drop_nans(data, ratio):
    '''
    Drops columns with NaN ratio above a certain ratio that the user chooses
    '''
    too_many_nans = []
    for i, k in enumerate(data.columns):
        array = data[k]
        nan_ratio = np.isnan(array).sum()/len(array)
        if nan_ratio > ratio:
            too_many_nans.append(k)

    data_wo_nans = data.drop(too_many_nans, axis=1)

    return data_wo_nans
