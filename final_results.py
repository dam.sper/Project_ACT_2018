#!/usr/bin/env python3

import random_forest as rf
import make_learning_set as mls
import decision_tree as dctree

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


def main():
    data_zphot = getPhotoz()

    skip = ['x', 'y', 'ra', 'dec', 'id', 'nchild', 'id_parent']
    list_colors = ['CH4', 'CH3', 'CH2', 'CH1', 'Ks', 'K',
                   'H', 'H2', 'H1', 'eJ', 'J3', 'J2', 'J1',
                   'z', 'I', 'R', 'G', 'U']
    colors = []
    for i, c_i in enumerate(list_colors):
        for c_j in list_colors[i:]:
            if c_i == c_j:
                continue
            colors.append((c_i, c_j, "F"))  # ratio fluxes

    data = getFullsample(data_zphot, skip=skip, colors=colors)

    data["in"] = dctree.cleanupData(data["in"])

    trained_forest = trainRandomforest(n_estimator=5.)

    z_photo_final = applyForest(data, trained_forest)

    plotResults(z_photo_final, data)


def getPhotoz():
    filenames = ["aegis-n2.deblend.v5.1.zout", "cosmos-1.deblend.v5.1.zout"]

    data = mls.readData(filenames)

    skip1 = ['id', 'z_spec', 'z_a', 'z_m1', 'chi_a', 'z_p', 'chi_p',
             'odds', 'l68', 'u68', 'l95', 'u95',
             'l99', 'u99', 'nfilt', 'q_z', 'z_peak', 'peak_prob', 'z_mc']
    data_zphot = mls.removeColumns(data, skip=skip1)
    return data_zphot


def getFullsample(data_zphot, skip=None, colors=None, add_noise=True,
                  ratio_nan=0.5):
    # Reading catalogs
    filenames = ["aegis-n2.deblend.v5.1.cat", "cosmos-1.deblend.v5.1.cat"]
    data = mls.readData(filenames)

    # Adding the photo_z to the dataframe
    data = data.assign(z_m2=pd.Series(data_zphot['z_m2']).values)

    # Applying the same treatment to the full dataframe
    data = mls.removeNaN(data, ratio_nan)
    data = mls.removeColumns(data, skip)
    data = mls.addColor(data, colors)
    data = mls.addNoise(data, add_noise)
    # Removing galaxies with z_spec and ditching the z_spec column
    data = removeSpecdata(data)
    # Removing galaxies with star_flag = 0
    data = removeStars(data)
    # Splitting into input and output dataset
    data = splitInputOutput(data)

    return data


def removeSpecdata(data):
    c = (data['z_spec'] > 0.)  # &(data['z_spec'] < 1.5)
    data = data[c]
    del data['z_spec']
    return data


def removeStars(data):
    c = (data['star_flag'] == 0)
    data = data[c]
    return data


def splitInputOutput(data):
    d = {}
    d["out"] = data["z_m2"]
    del data["z_m2"]
    d["in"] = data
    return d


def trainRandomforest(n_estimator):
    parameters = {
        "min_samples_split": 3,
        "presort": True,
    }
    trained_forest, train, test = rf.randomForestregressor(
        n_estimator, parameters)
    return trained_forest


def applyForest(data, forest):
    z_photo = []
    for i in range(len(forest)):
        tree = forest[i]
        z_photo.append(tree.predict(data['in']))

    z_photo_med = np.median(z_photo, axis=0)
    return z_photo_med


def plotResults(z, data):
    z_photo = data["out"]
    dz = z - z_photo
    # check distribution dz
    plt.figure()
    plt.hist(dz, 50, range=(-0.2, 0.2))
    plt.xlabel(r"$\Delta z$")
    plt.ylabel("Number of Cases")

    # check dz as function of z
    plt.figure()
    plt.plot(z_photo, dz, "o", ms=1)
    plt.ylim([-1, 1])
    plt.xlim([0, 1.5])
    plt.xlabel("Redshift Photo")
    plt.ylabel(r"$\Delta z$")

    # check z_ml as function of z_spectro
    plt.figure()
    plt.plot(z_photo, z, "o", ms=1)
    m = min(z_photo.min(), z.min())
    M = max(z_photo.max(), z.max())
    plt.plot([m, M], [m, M], '--k', lw=1, alpha=0.8)
    # plt.xlim([0,1.5])
    # plt.ylim([0,1.5])
    plt.xlabel("Photometric redshift")
    plt.ylabel("Photometric Redshift Machine Learning")

    plt.show()


if __name__ == '__main__':
        main()
