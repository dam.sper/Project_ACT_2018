#!/usr/bin/python3

import read_catalog as rc
import matplotlib.pyplot as plt

filenames = ["aegis-n2.deblend.v5.1.cat", "cosmos-1.deblend.v5.1.cat"]


def main():

    # read data
    data = readData()

    # plot data
    plotData(data)


def readData():
    data = []
    for f in filenames:
        tmp = rc.read_catalogs(f)
        data.append(tmp)

    return data


def plotData(data):
    for d in data:
        for field in d.keys():
            if field == "z_spec":
                continue

            plt.figure(field)
            plt.xlabel("Redshift")
            plt.ylabel(field)

            plt.plot(d["z_spec"], d[field], "o")

    plt.show()


if __name__ == "__main__":
    main()
