#!/usr/bin/env python3

import numpy as np
import sys
import matplotlib.pyplot as plt
import read_catalog as rc

def main():
    z_spec = getZspec()
    
    z_spec = z_spec[(z_spec > 0.)]#&(z_spec < 1.5)]
    
    plotZspec(z_spec,bins=100)


    

def getZspec():
    data_ae = rc.read_catalogs('aegis-n2.deblend.v5.1.cat')
    data_cos = rc.read_catalogs('cosmos-1.deblend.v5.1.cat')
    
    z_spec = np.append(data_ae['z_spec'],data_cos['z_spec'])
    
    return z_spec

def plotZspec(z_spec,**kwargs_hist):
    plt.hist(z_spec,**kwargs_hist)
    plt.xlabel('Spectroscopic redshift')
    plt.ylabel('Number of galaxies')
    plt.show()
    

if __name__ == "__main__":
    sys.exit(main())
