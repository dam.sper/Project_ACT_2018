#!/usr/bin/env python3

import read_catalog as rc

# standard libraries
import numpy as np
from sklearn.model_selection import train_test_split


def makeLearningSet(filenames=None, data=None, skip=None, colors=None,
                    add_noise=True,
                    ratio_training=0.8, ratio_nan=0.5, rand_seed=None):
    """
    Generate a training and a testing data set from a list of files.

    Parameters
    ----------

    filenames: list
        file to read
    skip: list
        fields to skip
    colors: list
        each item contains a tuple with two fields (first - second) along the type ('F' or 'M')
    ratio_training: float
        ratio of data for the training
    ratio_nan: float
        ratio of nan accepted
    rand_seed: int
        seed to use for random noise (None means no seed)

    Returns
    -------

    train: DataFrame
        Data to use for training
    test: DataFrame
        Data to use for testing

    unknown: DataFrame
        Data without z_spec
    """
    if filenames is None and data is None:
        raise Exception("You need to provide either 'data' or 'filenames'")

    if rand_seed is not None:
        np.random.seed(rand_seed)

    # read data
    if filenames is not None:
        data = readData(filenames)

        # cleanup data
        data = removeNaN(data, ratio_nan)

        data = removeColumns(data, skip)

    # randomize data
    data = addNoise(data, add_noise)

    # add colors
    data = addColor(data, colors)

    # split data and return
    train, test, unknown = splitTrainingTesting(data, ratio_training)

    train = splitInputOutput(train)
    test = splitInputOutput(test)

    return train, test, unknown


def append(data1, data2):
    # remove key in data1
    for i in data1.keys():
        if i not in data2:
            # print("Deleting %s" % i)
            del data1[i]

    # remove key in data2
    for i in data2.keys():
        if i not in data1:
            # print("Deleting %s" % i)
            del data2[i]

    # append
    data = data1.append(data2)
    return data


def readData(filenames):
    data = None
    for i, f in enumerate(filenames):
        tmp = rc.read_catalogs(f)
        tmp["dataset"] = i * np.ones(tmp.shape[0])

        # if no data yet, use this one
        if data is None:
            data = tmp
            continue

        data = append(data, tmp)

    return data


def removeNaN(data, ratio):
    return rc.drop_nans(data, ratio)


def removeColumns(data, skip):
    if skip is None:
        return data

    for field in skip:
        del data[field]

    return data


def fluxToMag(flux):
    mag = -2.5*np.log10(flux)+25.0
    return mag


def addColor(data, diff):
    if diff is None:
        return data

    for i, j, tpe in diff:
        if tpe == "F":
            name = i + "/" + j
            data[name] = data[i] / data[j]
        elif tpe == "M":
            name = i + "-" + j
            d_i = fluxToMag(data[i])
            d_j = fluxToMag(data[j])
            data[name] = d_i - d_j

        else:
            raise IOError("Type unknown %s" % tpe)

    return data


def addNoise(data, add_noise):
    if not add_noise:
        return data

    for field in data.keys():
        error = "e" + field
        if error not in data.keys():
            continue

        err = data[error]
        del data[error]
        width = "w" + field
        if width in data.keys():
            del data["w" + field]
        data[field] += np.random.normal(scale=np.abs(err))

    return data


def splitTrainingTesting(data, ratio):
    unknown = data[data.z_spec == -1]
    data = data[data.z_spec != -1]
    train, test = train_test_split(data, train_size=ratio, test_size=1-ratio)
    return train, test, unknown


def splitInputOutput(data):
    d = {}
    d["out"] = data["z_spec"]
    del data["z_spec"]
    d["in"] = data
    return d


if __name__ == "__main__":
    filenames = ["aegis-n2.deblend.v5.1.cat", "cosmos-1.deblend.v5.1.cat"]
    skip = ["id"]
    colors = [("H", "I", "F"), ("H1", "U", "M")]
    rand_seed = 1

    train, test, unknown = makeLearningSet(filenames, skip=skip, colors=colors,
                                           rand_seed=rand_seed)

    print(train["in"].keys())
