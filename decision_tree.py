#!/usr/bin/env python3

from make_learning_set import makeLearningSet

from sklearn import tree
import numpy as np
import matplotlib.pyplot as plt
from copy import deepcopy

parameters = {
    "min_samples_split": 3,
    "presort": True,
}


def main():
    train, test, unknown = generateData(rand_seed=None)

    train["in"] = cleanupData(train["in"])
    test["in"] = cleanupData(test["in"])

    trained_tree = trainTree(train, parameters)

    z, score = testTree(trained_tree, test)

    print("The score is %f (best -> 1.0)" % score)
    analysis(trained_tree, test, z)


def generateData(rand_seed=1, data=None, noise=True, colors=True):
    if colors:
        list_colors = ['CH4', 'CH3', 'CH2', 'CH1', 'Ks', 'K',
                       'H', 'H2', 'H1', 'eJ', 'J3', 'J2', 'J1',
                       'z', 'I', 'R', 'G', 'U']
        colors = []
        for i, c_i in enumerate(list_colors):
            for c_j in list_colors[i:]:
                if c_i == c_j:
                    continue
                colors.append((c_i, c_j, "F"))  # ratio fluxes
                # colors.append((c_i, c_j, "M"))  # diff magnitudes
    else:
        colors = []

    if data is None:
        filenames = ["aegis-n2.deblend.v5.1.cat", "cosmos-1.deblend.v5.1.cat"]

        skip = ['x', 'y', 'ra', 'dec', 'id', 'nchild', 'id_parent']

        return makeLearningSet(filenames, skip=skip, colors=colors,
                               rand_seed=rand_seed, add_noise=noise)

    else:
        tmp = deepcopy(data["in"])
        tmp["z_spec"] = data["out"]
        return makeLearningSet(data=tmp, add_noise=noise, rand_seed=rand_seed,
                               colors=colors)


def cleanupData(data):
    data = data.astype(np.float32)
    data = data.replace(np.inf, -1)
    data = data.replace(-np.inf, -1)
    data = data.replace(np.nan, -1)
    return data


def trainTree(train, parameters):
    tr = tree.DecisionTreeRegressor(**parameters)
    tr.fit(train["in"], train["out"])
    return tr


def testTree(tr, test):
    score = tr.score(test["in"], test["out"])
    z_photo = tr.predict(test["in"])
    return z_photo, score


def plotFeaturesImportance(fields, tr):
    ind = np.argsort(tr.feature_importances_)
    features = tr.feature_importances_[ind]
    fields = fields[ind]

    plt.figure()
    ind = range(len(fields))
    plt.bar(ind, features, align='center')
    plt.xticks(ind, fields, rotation=90)


def analysis(trained_tree, test, z):
    plotFeaturesImportance(test["in"].keys(), trained_tree)

    z_spec = test["out"]
    dz = z - z_spec

    # check distribution dz
    plt.figure()
    plt.hist(dz, 50, range=(-1, 1))
    plt.xlabel(r"$\Delta z$")
    plt.ylabel("Number of Cases")

    # check dz as function of z
    plt.figure()
    plt.plot(z_spec, z-z_spec, "o", ms=1)
    plt.xlabel("Redshift")
    plt.ylabel(r"$\Delta z$")

    # check z_ml as function of z_spectro
    plt.figure()
    m = min(z_spec.min(), z.min())
    M = max(z_spec.max(), z.max())
    plt.plot([m, M], [m, M], '--k', lw=1, alpha=0.8)
    plt.plot(z_spec, z, "o", ms=1)
    plt.xlabel("Redshift")
    plt.ylabel("Redshift Machine Learning")

    plt.show()


def testMinSamplesSplit():
    train, test, unknown = generateData(rand_seed=None)

    train["in"] = cleanupData(train["in"])
    test["in"] = cleanupData(test["in"])

    params = parameters

    min_samples_split = np.arange(20).astype(int) + 2
    score = []
    for i in min_samples_split:
        params["min_samples_split"] = i
        trained_tree = trainTree(train, params)

        z, tmp = testTree(trained_tree, test)
        score.append(tmp)

    plt.figure()
    plt.plot(min_samples_split, score)
    plt.show()


def testMaxLeafNodes():
    train, test, unknown = generateData(rand_seed=None)

    train["in"] = cleanupData(train["in"])
    test["in"] = cleanupData(test["in"])

    params = parameters

    max_leaf_nodes = np.logspace(2, 4, 10).astype(int)
    score = []
    for i in max_leaf_nodes:
        params["max_leaf_nodes"] = i
        trained_tree = trainTree(train, params)

        z, tmp = testTree(trained_tree, test)
        score.append(tmp)

    plt.figure()
    plt.plot(max_leaf_nodes, score)
    plt.show()


def testPresort():
    train, test, unknown = generateData(rand_seed=None)

    train["in"] = cleanupData(train["in"])
    test["in"] = cleanupData(test["in"])

    params = parameters

    presort = [False, True]
    score = []
    for i in presort:
        params["presort"] = i
        trained_tree = trainTree(train, params)

        z, tmp = testTree(trained_tree, test)
        score.append(tmp)

    plt.figure()
    plt.plot(presort, score)
    plt.show()


if __name__ == "__main__":
    # testMinSamplesSplit()

    # testMaxLeafNodes()

    # testPresort()

    main()
